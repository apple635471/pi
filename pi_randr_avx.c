#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <x86intrin.h>

#define RANDMAX 4611686014132420610

typedef struct payload{
	long long runs;
	long long pi_estimate;
	pthread_mutex_t lock;
}payload;

void* estimate_pi( void* arg ){
	payload *input = ( payload* ) arg;
	long long number_of_tosses = (*input).runs;
	long long number_in_circle = 0;
	unsigned int seed = ( unsigned int )pthread_self() ^ time(NULL);

	__m256i output	 = _mm256_set_epi64x( 0, 0, 0, 0 );
	__m256i mask2047   = _mm256_set_epi64x( 2047, 2047, 2047, 2047 );
	__m256i mask1023   = _mm256_set_epi64x( 1023, 1023, 1023, 1023 );
	__m256i static_	= _mm256_set_epi64x( 12345, 12345, 12345, 12345 );
	__m256i seed_x	 = _mm256_set_epi64x( seed,	seed*2,   seed*3,  seed*4 ); //need mod
	__m256i seed_y	 = _mm256_set_epi64x( seed*16, seed*15, seed*14, seed*13 ); //need mod
	__m256i randsquare = _mm256_set_epi64x( RANDMAX, RANDMAX, RANDMAX, RANDMAX );

	for( long long toss = 0; toss < number_of_tosses; toss += 1 ){
		__m256i x_next  = seed_x;
		__m256i y_next  = seed_y;
		__m256i x_result;
		__m256i y_result;
		__m256i x_temp;
		__m256i y_temp;
		__m256i temp;
		__m256i x;
		__m256i y;

		// next *= 1103515245;
		temp	 = _mm256_set_epi64x( 1103515245, 1103515245, 1103515245, 1103515245 );
		x_next   = _mm256_mul_epu32 ( temp, x_next );
		y_next   = _mm256_mul_epu32 ( temp, y_next );

		// next += 12345;
		x_next   = _mm256_add_epi64 ( static_, x_next );
		y_next   = _mm256_add_epi64 ( static_, y_next );

		// result = (unsigned int) (next / 65536) % 2048;
		x_temp   = _mm256_srli_epi64( x_next, 16 );
		x_result = _mm256_and_si256 ( x_temp, mask2047 );
		y_temp   = _mm256_srli_epi64( y_next, 16 );
		y_result = _mm256_and_si256 ( y_temp, mask2047 );

		// next *= 1103515245;
		temp	 = _mm256_set_epi64x( 1103515245, 1103515245, 1103515245, 1103515245 );
		x_next   = _mm256_mul_epu32 ( temp, x_next );
		y_next   = _mm256_mul_epu32 ( temp, y_next );

		// next += 12345;
		x_next   = _mm256_add_epi64 ( static_, x_next );
		y_next   = _mm256_add_epi64 ( static_, y_next );

		// result <<= 10;
		x_result = _mm256_slli_epi64( x_result, 10 );
		y_result = _mm256_slli_epi64( y_result, 10 );

		// result ^= (unsigned int) (next / 65536) % 1024;
		x_temp   = _mm256_srli_epi64( x_next, 16 );
		x_temp   = _mm256_and_si256 ( x_temp, mask1023 );
		x_result = _mm256_xor_si256 ( x_temp, x_result );
		y_temp   = _mm256_srli_epi64( y_next, 16 );
		y_temp   = _mm256_and_si256 ( y_temp, mask1023 );
		y_result = _mm256_xor_si256 ( y_temp, y_result );

		// next *= 1103515245;
		temp	 = _mm256_set_epi64x( 1103515245, 1103515245, 1103515245, 1103515245 );
		x_next   = _mm256_mul_epu32 ( temp, x_next );
		y_next   = _mm256_mul_epu32 ( temp, y_next );

		// next += 12345;
		x_next   = _mm256_add_epi64 ( static_, x_next );
		y_next   = _mm256_add_epi64 ( static_, y_next );

		// result <<= 10;
		x_result = _mm256_slli_epi64( x_result, 10 );
		y_result = _mm256_slli_epi64( y_result, 10 );

		// result ^= (unsigned int) (next / 65536) % 1024;
		x_temp   = _mm256_srli_epi64( x_next, 16 );
		x_temp   = _mm256_and_si256 ( x_temp, mask1023 );
		x_result = _mm256_xor_si256 ( x_temp, x_result );
		y_temp   = _mm256_srli_epi64( y_next, 16 );
		y_temp   = _mm256_and_si256 ( y_temp, mask1023 );
		y_result = _mm256_xor_si256 ( y_temp, y_result );

		// *seed = next;
		seed_x = x_next;
		seed_y = y_next;

		//return result;
		x   = _mm256_mul_epu32  ( x_result, x_result );
		y   = _mm256_mul_epu32  ( y_result, y_result );
		x   = _mm256_add_epi64  ( x, y );
		x   = _mm256_cmpgt_epi64( randsquare, x );

		//reuse of x_next and y_next
		x_next = _mm256_srli_epi64 ( x, 63 );
		output = _mm256_add_epi64  ( x_next, output );
	}
	for( int i = 0; i < 4; ++i ){
		number_in_circle += _mm256_extract_epi64( output, i );
	}
	pthread_mutex_lock( &( (*input).lock ) );
	(*input).pi_estimate += number_in_circle;
	pthread_mutex_unlock( &( (*input).lock ) );
}

int main(int argc, char *argv[]){
	if( argc != 3 ){
		fprintf(stderr, "Two arguments needed\n");
		exit(EXIT_FAILURE);
	}
	else{
		payload start;
		start.pi_estimate = 0;
		int threads = atoi( argv[argc - 2 ] );
		long long runs = atoll( argv[argc - 1 ] ) / threads;
		start.runs = runs >> 2 + ( ( runs & 0x2 ) | ( runs & 0x1 ) );
		//printf("Running %lld RUNs on %d threads\n", start.runs, threads);
		pthread_t thread[ threads ];
		pthread_mutex_init( &start.lock, NULL );
		pthread_mutex_lock( &( start.lock ) );

		int i;
		for( i = 0; i < threads ; ++i ){
			if( pthread_create( &thread[i], NULL, estimate_pi, ( void * )&start ) ){
				fprintf(stderr, "Error creating thread\n");
				exit(EXIT_FAILURE);
			}
		}

		pthread_mutex_unlock( &( start.lock ) );

		for( i = 0; i < threads ; ++i ){
			if( pthread_join( thread[i], NULL ) ){
				fprintf(stderr, "Error join thread\n");
				exit(EXIT_FAILURE);
			}
		}

		printf( "Estimates PI = %lf\n", start.pi_estimate / (double)( start.runs * threads ) );
	}
}
