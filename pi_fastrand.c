#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <emmintrin.h>
#include <nmmintrin.h>
#define RAND_SQR 4611686014132420610 // (2^31-1)^2+1

typedef struct payload{
	long long runs;
	long long pi_estimate;
	pthread_mutex_t lock;
}payload;

void* estimate_pi( void* arg ){
	payload *input = ( payload* ) arg;
	long long number_of_tosses = (*input).runs;
	long long number_in_circle = 0;
	unsigned int seed = ( unsigned int )pthread_self() ^ time(NULL);
	long long toss = 0;

	__m128i mul   = _mm_set_epi32(    1103515245,   1103515245,    1103515245,   1103515245);
	__m128i add   = _mm_set_epi32(         12345,        12345,         12345,        12345);

	__m128i stora = _mm_set_epi32(	  2801247362,	 372922539, rand_r(&seed),	 362436069 );
	__m128i storb = _mm_set_epi32( rand_r(&seed),	2939983983,    3839279122,  1487178804 );

	__m128i one	   = _mm_set_epi64x(		  1,		 1 );
	__m128i output = _mm_set_epi64x(		  0,		 0 );
	__m128i max	   = _mm_set_epi64x(   RAND_SQR,  RAND_SQR );
	__m128i mask   = _mm_set_epi64x( 2147483647,2147483647 );

	for( toss = 0; toss < number_of_tosses; ++toss ){
		__m128i ta,tb,tc,td;

		// rand_next = rand_next * 1103515245 + 12345;
		stora = _mm_mullo_epi32( stora, mul );
		storb = _mm_mullo_epi32( storb, mul );
		stora = _mm_add_epi32  ( stora, add );
		storb = _mm_add_epi32  ( storb, add );

		// Square and sum
		ta = _mm_mul_epi32    ( stora,   stora );
		tb = _mm_mul_epi32    ( storb,   storb );

		tc = _mm_shuffle_epi32( stora, 177 );
		td = _mm_shuffle_epi32( storb, 177 );
		tc = _mm_mul_epi32    ( tc,   tc );
		td = _mm_mul_epi32    ( td,   td );

		ta = _mm_add_epi64    ( ta,   tb );
		tc = _mm_add_epi64    ( tc,   td );

		//check if square less than 1 ( without divide )
		tb = _mm_cmpgt_epi64( max,   ta );
		td = _mm_cmpgt_epi64( max,   tc );

		// convert result to one for adding
		ta = _mm_and_si128  ( tb,  one );
		tc = _mm_and_si128  ( td,  one );

		// accumulate
		output = _mm_add_epi64( output, ta );
		output = _mm_add_epi64( output, tc );
	}
	pthread_mutex_lock( &( (*input).lock ) );
	(*input).pi_estimate += _mm_extract_epi64( output, 0 ) + _mm_extract_epi64( output, 1 );
	pthread_mutex_unlock( &( (*input).lock ) );
}

int main(int argc, char *argv[]){
	if( argc != 3 ){
		fprintf(stderr, "Two arguments needed\n");
		exit(EXIT_FAILURE);
	}
	else{
		payload start;
		start.pi_estimate = 0;
		int threads = atoi( argv[argc - 2 ] );
		long long runs = atoll( argv[argc - 1 ] ) / threads;
		start.runs = ( runs >> 2 ) + ( runs & 1 | runs & 2 );
		//printf("Running %lld RUNs on %d threads\n", start.runs, threads);
		pthread_t thread[ threads ];
		pthread_mutex_init( &start.lock, NULL );
		pthread_mutex_lock( &( start.lock ) );

		int i;
		for( i = 0; i < threads ; ++i ){
			if( pthread_create( &thread[i], NULL, estimate_pi, ( void * )&start ) ){
				fprintf(stderr, "Error creating thread\n");
				exit(EXIT_FAILURE);
			}
		}

		pthread_mutex_unlock( &( start.lock ) );

		for( i = 0; i < threads ; ++i ){
			if( pthread_join( thread[i], NULL ) ){
				fprintf(stderr, "Error join thread\n");
				exit(EXIT_FAILURE);
			}
		}

		printf( "Estimates PI %lld / %lld = %lf\n", 4 * start.pi_estimate, 4 * start.runs * threads, start.pi_estimate / (double)( start.runs * threads ) ); // 4 * start.pi_estimate / (double)( 4 * start.runs * threads ) )
	}
}
