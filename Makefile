CC   := gcc
EXE   = pi
OBJs  = pi_randr_avx.o pi_randr.o pi_xorshift128.o pi_xorshift128_sse.o pi_fastrand.o pi_sub_comp_fastrand.o
FLAGS = -O2 -pthread -mavx2 -msse -msse4
RUNS  = 1000000000

all  : $(OBJs)
	$(CC) -o pi_randr_avx $(FLAGS) pi_randr_avx.o
	$(CC) -o pi_randr $(FLAGS) pi_randr.o
	$(CC) -o pi_xorshift128 $(FLAGS) pi_xorshift128.o
	$(CC) -o pi_xorshift128_sse $(FLAGS) pi_xorshift128_sse.o
	$(CC) -o pi_fastrand $(FLAGS) pi_fastrand.o
	$(CC) -o pi_sub_comp_fastrand $(FLAGS) pi_sub_comp_fastrand.o

%.o : %.c
	$(CC) -c $(FLAGS) -o $@ $<

.PHONY: clean run

clean:
	rm *.o pi_randr pi_randr_avx pi_xorshift128 pi_xorshift128_sse pi_fastrand pi_sub_comp_fastrand

run:
	for i in $$(seq 4); do time ./pi_randr_avx $$i $(RUNS) ; done
	for i in $$(seq 4); do time ./pi_randr $$i $(RUNS) ; done
	for i in $$(seq 4); do time ./pi_xorshift128_sse $$i $(RUNS) ; done
	for i in $$(seq 4); do time ./pi_xorshift128 $$i $(RUNS) ; done
	for i in $$(seq 4); do time ./pi_sub_comp_fastrand $$i $(RUNS) ; done
	for i in $$(seq 4); do time ./pi_fastrand $$i $(RUNS) ; done
