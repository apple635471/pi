#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct payload{
	long long runs;
	long long pi_estimate;
	pthread_mutex_t lock;
}payload;

void* estimate_pi( void* arg ){
	payload *input = ( payload* ) arg;
	long long number_of_tosses = (*input).runs;
	long long number_in_circle = 0;
	unsigned int seed = time( NULL );

	long long toss = 0;
	for( toss = 0; toss < number_of_tosses; ++toss ){
		double x = rand_r(&seed) / ( ( double )RAND_MAX ); // can optimize
		double y = rand_r(&seed) / ( ( double )RAND_MAX ); // can optimize
		number_in_circle += ( ( x*x + y*y ) <= 1 );
	}
	pthread_mutex_lock( &( (*input).lock ) );
	(*input).pi_estimate += number_in_circle;
	pthread_mutex_unlock( &( (*input).lock ) );
}

int main(int argc, char *argv[]){
	if( argc != 3 ){
		fprintf(stderr, "Two arguments needed\n");
		exit(EXIT_FAILURE);
	}
	else{
		payload start;
		start.pi_estimate = 0;
		int threads = atoi( argv[argc - 2 ] );
		start.runs = atoll( argv[argc - 1 ] ) / threads;
		//printf("Running %lld RUNs on %d threads\n", start.runs, threads);
		pthread_t thread[ threads ];
		pthread_mutex_init( &start.lock, NULL );
		pthread_mutex_lock( &( start.lock ) );

		int i;
		for( i = 0; i < threads ; ++i ){
			if( pthread_create( &thread[i], NULL, estimate_pi, ( void * )&start ) ){
				fprintf(stderr, "Error creating thread\n");
				exit(EXIT_FAILURE);
			}
		}

		pthread_mutex_unlock( &( start.lock ) );

		for( i = 0; i < threads ; ++i ){
			if( pthread_join( thread[i], NULL ) ){
				fprintf(stderr, "Error join thread\n");
				exit(EXIT_FAILURE);
			}
		}

		printf( "Estimates PI = %lf\n", 4 * start.pi_estimate / (double)( start.runs * threads ) ); // start.runs * threads = total runs
	}
}
