#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <emmintrin.h>
#include <nmmintrin.h>
#define RAND_SQR 1073676289 // ( 2^15 - 1 )^2 + 1

typedef struct payload{
	long long runs;
	long long pi_estimate;
	pthread_mutex_t lock;
}payload;

void* estimate_pi( void* arg ){
	payload *input = ( payload* ) arg;
	long long number_of_tosses = (*input).runs;
	long long number_in_circle = 0;
	unsigned int seed = ( unsigned int )pthread_self() ^ time(NULL);
	long long toss = 0;

	const __m128i mul   = _mm_set_epi32 (   1103515245,   1103515245,   1103515245,   1103515245);
	const __m128i add   = _mm_set_epi32 (        12345,        12345,        12345,        12345);
	const __m128i one   = _mm_set_epi32 (            1,            1,            1,            1);

	//__m128i stora       = _mm_set_epi32 (	1103515245,    372922539,    751699623,    362436069);
	//__m128i storb       = _mm_set_epi32 ( 2014985866,    575704611,     22557521,   1084531195);
	__m128i stora       = _mm_set_epi32 (rand_r(&seed),rand_r(&seed),rand_r(&seed),rand_r(&seed));
	__m128i storb       = _mm_set_epi32 (rand_r(&seed),rand_r(&seed),rand_r(&seed),rand_r(&seed));
	__m128i max         = _mm_set_epi32 (    -RAND_SQR,    -RAND_SQR,    -RAND_SQR,    -RAND_SQR);
	__m128i mask        = _mm_set_epi16 ( 32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767);
	__m128i output      = _mm_set_epi64x(            0,           0);

	for( toss = 0; toss < number_of_tosses; ++toss ){
		__m128i ta1,ta2,tb1,tb2;

		// rand_next = rand_next * 1103515245 + 12345;
		// generated 128bit rand number
		stora = _mm_mullo_epi32  ( stora,   mul );
		storb = _mm_mullo_epi32  ( storb,   mul );

		stora = _mm_add_epi32    ( stora,   add );
		storb = _mm_add_epi32    ( storb,   add );

		// converted into 8 * 16 rand number
		stora = _mm_and_si128    ( stora,  mask );
		storb = _mm_and_si128    ( storb,  mask );

		// inner multiple 8 * 16 to 4 * ( 2 *( 16 bit squared ) rand sum )
		ta1   = _mm_madd_epi16   ( stora, stora );
		tb1   = _mm_madd_epi16   ( storb, storb );

		// minus ( randmax's square + 1 ), then signbit is equivalent to rand1^2 + rand2^2 <= randmax^2
		ta1   = _mm_add_epi32    (   max,   ta1 );
		tb1   = _mm_add_epi32    (   max,   tb1 );

		// shift 31bits to get signbit
		ta1   = _mm_srli_epi32   (   ta1,    31 );
		tb1   = _mm_srli_epi32   (   tb1,    31 );

		// in order to sum four results, the original order 3,2,1,0 is shuffled into 0,1,2,3
		ta2   = _mm_shuffle_epi32 (   ta1,    27 );
		tb2   = _mm_shuffle_epi32 (   tb1,    27 );

		// add the results of orignal and shuffled result, thus the sum is xx,xx,1+2,0+3 ( xx is discarded )
		ta1   = _mm_add_epi32     (   ta1,   ta2 );
		tb1   = _mm_add_epi32     (   tb1,   tb2 );

		// save the two groups of independent results
		ta1   = _mm_add_epi32     (   ta1,   tb1 );

		// extend the result to 64bit in case of accumulating latter overflows
		tb1  = _mm_cvtepi32_epi64 (          ta1 );

		// accumulate
		output = _mm_add_epi64    (   tb1, output);

	}
	pthread_mutex_lock( &( (*input).lock ) );
	(*input).pi_estimate += _mm_extract_epi64( output, 0 ) + _mm_extract_epi64( output, 1 );
	pthread_mutex_unlock( &( (*input).lock ) );
}

int main(int argc, char *argv[]){
	if( argc != 3 ){
		fprintf(stderr, "Two arguments needed\n");
		exit(EXIT_FAILURE);
	}
	else{
		payload start;
		start.pi_estimate = 0;
		int threads = atoi( argv[argc - 2 ] );
		long long runs = atoll( argv[argc - 1 ] ) / threads;
		start.runs = ( runs >> 3 ) + ( runs & 1 | runs & 2 | runs & 4 );
		//printf("Running %lld RUNs on %d threads\n", start.runs, threads);
		pthread_t thread[ threads ];
		pthread_mutex_init( &start.lock, NULL );
		//pthread_mutex_lock( &( start.lock ) );

		int i;
		for( i = 0; i < threads ; ++i ){
			if( pthread_create( &thread[i], NULL, estimate_pi, ( void * )&start ) ){
				fprintf(stderr, "Error creating thread\n");
				exit(EXIT_FAILURE);
			}
		}

		//pthread_mutex_unlock( &( start.lock ) );

		for( i = 0; i < threads ; ++i ){
			if( pthread_join( thread[i], NULL ) ){
				fprintf(stderr, "Error join thread\n");
				exit(EXIT_FAILURE);
			}
		}

		printf( "Estimates PI %lld / %lld = %lf\n", 4 * start.pi_estimate, 8 * start.runs * threads, start.pi_estimate / (double)( 2 * start.runs * threads ) ); // 4 * start.pi_estimate / (double)( 4 * start.runs * threads ) )
	}
}
